# This file is part of dolphin-plugins-misc.

# dolphin-plugins-misc is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.

# dolphin-plugins-misc is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with dolphin-plugins-misc.  If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2021-2022, Maciej Barć <xgqt@riseup.net>
# Licensed under the GNU GPL v3 License
# SPDX-License-Identifier: GPL-3.0-only


SERVICESDIR	?=	~/.local/share/kservices5/


all:
	@echo "Try install or remove"


install:
	mkdir -p $(SERVICESDIR)
	install -v -t $(SERVICESDIR) src/clean-metadata.desktop
	install -v -t $(SERVICESDIR) src/open-emacs.desktop
	install -v -t $(SERVICESDIR) src/show-metadata.desktop

remove:
	rm $(SERVICESDIR)/clean-metadata.desktop
	rm $(SERVICESDIR)/open-emacs.desktop
	rm $(SERVICESDIR)/show-metadata.desktop
